let firstChange = document.getElementById("myTitle");
console.log(firstChange.textContent);
firstChange.textContent = "H1 is changed";


let secondChange = document.getElementsByClassName("paragraph")[0];
console.log(secondChange.textContent);
secondChange.textContent = "First paragraph is changed"


let thirdChange = document.getElementsByClassName("paragraph")[1];
console.log(thirdChange.textContent);
thirdChange.textContent = "Second paragraph is changed";


let fourthChange = document.getElementsByTagName("text")[0];
console.log(fourthChange.textContent);
fourthChange.textContent = "Also this text is changed";

let fifthChange = document.getElementsByTagName("h1")[1];
console.log(fifthChange.textContent);
fifthChange.textContent = "Second h1 change";


let lastChange = document.getElementsByTagName("h3")[0];
console.log(lastChange.textContent);
lastChange.textContent = "Last change,that's it";