let numbers = [1, 2, 3, 4, 5];

    
    function printNumbers() {
        let list = document.getElementById("numberList");
            list.innerHTML = ""; 

            numbers.forEach(function(number) {
                let listItem = document.createElement("li");
                listItem.textContent = number;
                list.appendChild(listItem);
            });
        }

        function printSum() {
            let sum = numbers.reduce(function(acc, val) {
                return acc + val;
            }, 0);
            document.getElementById("sum").textContent = "Sum is: " + sum;
        }

        printNumbers();
        printSum();