function createTable() {
    let rowCount = +prompt("Write how many rows you want!");
    let columnCount = +prompt("Write how many columns you want!");

    let table = document.createElement("table");
    table.style.border = "2px solid violet";

    document.body.appendChild(table);

    for (let i = 1; i <= rowCount; i++) {
        let newRow = document.createElement('tr');
        for (let j = 1; j <= columnCount; j++) {
            let newColumn = document.createElement('td');
            newColumn.textContent = "Row-" + i + ", Column-" + j;
            newColumn.style.border = "2px solid violet";
            newColumn.style.padding = "15px";
            newRow.appendChild(newColumn);
        }
        table.appendChild(newRow);
    }
}

createTable();