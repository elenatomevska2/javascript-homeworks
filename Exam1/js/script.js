let totalTasks = 0;
let doneTasks = 0;
let leftTasks = 0;

document.getElementById("addTask").addEventListener("click", addTask);

    function addTask() {
        let taskInput = document.getElementById("taskInput");
        let taskText = taskInput.value.trim();
        if (taskText !== "") {
            totalTasks++;
            leftTasks++;
            updateTaskCount();
            let tasksList = document.getElementById("tasksList");
            let li = document.createElement("li");
            li.textContent = taskText;
            li.addEventListener("click", function() {
                toggleTask(this);
            });
            tasksList.appendChild(li);
            taskInput.value = "";
        }
    }

    function toggleTask(task) {
        if (!task.classList.contains("done")) {
            task.classList.add("done");
            leftTasks--;
            doneTasks++;
        } else {
            task.classList.remove("done");
            leftTasks++;
            doneTasks--;
        }
        updateTaskCount();
    }

    function updateTaskCount() {
        document.getElementById("totalTasks").textContent = totalTasks;
        document.getElementById("leftTasks").textContent = leftTask;
        document.getElementById("doneTasks").textContent = doneTask;
    }
