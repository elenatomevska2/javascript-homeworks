// Constuctor function 1
function Book(title, author, image, price, quantity, description) {
    this.title = title;
    this.author = author;
    this.image = image;
    this.price = price;
    this.quantity = quantity;
    this.description = description;
}

// COnstructor function 2
function BookStore(name) {
    this.name = name;
    this.books = [];
    this.shoppingCard = [];
    this.wishlist = [];
    this.shoppingGroup = document.querySelector('.shopping-group'); 
    this.shoppingGroup.style.display = 'none';
}


BookStore.prototype.addBook = function(book) {
    this.books.push(book);

    this.shoppingGroup.style.display = 'block';
    updateBooksList();
};




BookStore.prototype.addToShoppingCard = function(book) {
    if (book.quantity > 0) {
        this.shoppingCard.push(book);
        book.quantity--; 
        
        updateShoppingCart();
    } else {
        console.log("The book is out of stock");
    }
};


BookStore.prototype.addToWishlist = function(book) {
    this.wishlist.push(book);


    updateWishlist();
};

// this is for remove from shopping card
BookStore.prototype.removeFromShoppingCard = function(book) {
    const index = this.shoppingCard.indexOf(book);
    if (index !== -1) {
        this.shoppingCard.splice(index, 1);
        book.quantity++; 


        updateShoppingCart();
    }
};

//this is for remove from wishlist
BookStore.prototype.removeFromWishlist = function(book) {
    const index = this.wishlist.indexOf(book);
    if (index !== -1) {
        this.wishlist.splice(index, 1);
        

        updateWishlist();
    }
};


const myBookStore = new BookStore("My Book Store");


function createShoppingCardItem(book, index) {
    const li = document.createElement("li");
    li.setAttribute("id", index);

    li.innerHTML = `
        ${book.title} ${book.author} ${book.price}
        <button class="remove-book-cart" data-id="${index}">Remove book</button>
    `;

    return li;
}


function updateShoppingCart() {
    const shoppingCart = document.getElementById("shopping-cart");
    shoppingCart.innerHTML = ""; 

    myBookStore.shoppingCard.forEach(function(book, index) {
        const li = createShoppingCardItem(book, index);

        
        li.querySelector(".remove-book-cart").addEventListener("click", function() {
            const index = this.getAttribute("data-id");
            const book = myBookStore.shoppingCard[index];
            myBookStore.removeFromShoppingCard(book);
        });

        shoppingCart.appendChild(li);
    });
};


function updateWishlist() {
    const wishlist = document.getElementById("wishlist");
    wishlist.innerHTML = "";

    myBookStore.wishlist.forEach(function(book, index) {
        const li = document.createElement("li");
        li.setAttribute("id", index); 
        li.textContent = `${book.title} ${book.author} ${book.price}`;



        const removeBtn = document.createElement("a");
        removeBtn.setAttribute("href", "#");
        removeBtn.setAttribute("class", "remove-book-wish");
        removeBtn.setAttribute("data-id", index);
        removeBtn.textContent = "Remove";
        removeBtn.addEventListener("click", function() {
            const index = this.getAttribute("data-id");
            const book = myBookStore.wishlist[index];
            myBookStore.removeFromWishlist(book);

        });
        li.appendChild(removeBtn);

        wishlist.appendChild(li);
    });
};

//  add button
document.getElementById("add-btn").addEventListener("click", function(e) {
    e.preventDefault(); 


    const title = document.getElementById("title").value;
    const author = document.getElementById("author").value;
    const image = document.getElementById("thumbnail").value;
    const price = parseFloat(document.getElementById("price").value);
    const quantity = parseInt(document.getElementById("quantity").value);
    const description = document.getElementById("description").value;

    
    const newBook = new Book(title, author, image, price, quantity, description);

    
    myBookStore.addBook(newBook);
});


function updateBooksList() {
    const booksList = document.getElementById("books-list");
    booksList.innerHTML = ""; 

    myBookStore.books.forEach(function(book, index) {
        const li = document.createElement("li");
        li.setAttribute("id", index);

        li.innerHTML = `
            <div class="card-title">${book.title}</div>
            <div class="card-img"><img src="${book.image}"></div>
            <div class="card-description">${book.description}</div>
            <div class="card-price">Price: ${book.price}</div>
            <div class="card-quantity">Quantity: ${book.quantity}</div>
            <div class="card-actions"> 
                <button class="buy-btn btn buy" data-id="${index}">Buy</button> 
                <button class="wishlist-btn btn" data-id="${index}">Add to Wishlist</button>
            </div>
        `;

        booksList.appendChild(li);

        
        li.querySelector(".buy-btn").addEventListener("click", function() {
            const index = this.getAttribute("data-id");
            const book = myBookStore.books[index];
            myBookStore.addToShoppingCard(book);
        });

        li.querySelector(".wishlist-btn").addEventListener("click", function() {
            const index = this.getAttribute("data-id");
            const book = myBookStore.books[index];
            myBookStore.addToWishlist(book);
        });
    });
}

updateBooksList(); 